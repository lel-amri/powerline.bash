#!/usr/bin/env bash
#
# Teste interactivement divers segments avec des données ad-hoc.
#
# Usage: bin/tests.sh [SEGMENT ...]
#
# Éventuellement, prends une capture d'écran du résultat si CAPTURE est défini.
#
# J'exécute ce script à la racine du projet, dans un terminal Tilix, palette de
# couleurs Material, police JetBrains Mono 14, icons-in-terminal, . La capture
# d'écran est faite avec gnome-screenshot, et le découpage avec convert
# d'imagemagick. Voir la fonction capturer() pour le détail.
#
set -eu

moi="$(readlink -e "$0")"
racine="${moi%/*/*}"
test -f "$racine/powerline.bash"
docsdir="$racine/docs"

cd "$racine"

icons_origin="${POWERLINE_ICONS-nerd-fonts}"

main() {
	mapfile -t declarations < <(list_tests)

	if [ "${1-}" = "?" ] ; then
		echo "Liste des tests disponibles:" >&2
		echo >&2
		printf "  %s\n" "${declarations[@]}"
		echo >&2
		echo "Usage: $0 [TEST ...]" >&2
		echo >&2
		exit
	fi

	attendus=("$@")

	if [ "${#attendus[@]}" -gt 0 ] ; then
		tests=("${attendus[@]}")
	else
		tests=("${declarations[@]}")
	fi

	# Tester les dépendances.
	if [ -v CAPTURE ] ; then
		type -p gnome-screenshot
		type -p convert
	fi
	type -p docker-compose
	type -p git
	type -p kind
	type -p kubectl

	# Chargement initial de powerline.bash
	# shellcheck source=/dev/null
	. "$racine/powerline.bash"
	reset

	teardown
	setup

	for test_ in "${tests[@]}" ; do
		fonction="$(declare -F | grep -Po " \Ktest_.+_$test_")"
		"$fonction"
	done
}


list_tests() {
	declare -F | grep ' test_' | sort | grep -Po 'test_.+_\K.+'
}

reset() {
	unset GIT_DIR GIT_WORK_TREE ${!POWERLINE_*}
	POWERLINE_ICONS=$icons_origin
	__powerline_init
}

setup() {
	# Dépôt git bidon pour avoir une branche master propre.
	mkdir -p /tmp/testgit
	git init --quiet /tmp/testgit
}

teardown() {
	rm -rf /tmp/testgit
}

test_00_tout() {
	# Exécuter le segment git sur un autre dossier que le segment pwd.
	export GIT_WORK_TREE=/tmp/testgit
	export GIT_DIR=$GIT_WORK_TREE/.git

	titre       "T O U T"

	cd ~
	generer "pwd"
	commande cd src/bersace/powerline.bash
	generer "pwd git"
	commande . .venv/bin/activate
	VIRTUAL_ENV="$PWD/.venv"
	generer "pwd python git status"
	commande false
	generer "pwd python git status" 1

	for _ in {0..3} ; do
		echo
		echo
		nombre_prompts=$((nombre_prompts+1))
	done
	capturer screenshot.png


	unset ${!POWERLINE_*}
	POWERLINE_DIRECTION=rtl
	POWERLINE_ICONS=$icons_origin
	__powerline_init
	echo
	generer "pwd python git status"
	capturer screenshot-rtl.png

	unset ${!POWERLINE_*}
	POWERLINE_ICONS=$icons_origin
	POWERLINE_ONELINE=true
	POWERLINE_CHASSIS=laptop
	__powerline_init
	pushd /usr/lib/terminfo/ &>/dev/null
	echo
	generer "hostname pwd python git status"
	echo
	popd &>/dev/null
	capturer screenshot-oneline.png

	reset
}


test_01_sep() {
	# Exécuter le segment git sur un autre dossier que le segment pwd.
	export GIT_WORK_TREE=/tmp/testgit
	export GIT_DIR=$GIT_WORK_TREE/.git

	titre          "S E P"

	unset ${!POWERLINE_*}
	POWERLINE_ICONS=$icons_origin
	POWERLINE_ONELINE=true
	declare -A POWERLINE_ICONS_OVERRIDES
	POWERLINE_ICONS_OVERRIDES=(
		[sep]=$'\uE0B4 '
		[sep-fin]=$'\uE0B5 '
	)
	__powerline_init

	echo
	generer "pwd python git status"
	echo
	capturer screenshot-sep.png


	reset
}


test_01_hostname() {

	titre       "H O S T N A M E"

	USER=utilisateur
	HOSTNAME=ma-machine
	POWERLINE_CHASSIS=desktop
	generer hostname
	HOSTNAME=machine-virtuelle
	POWERLINE_CHASSIS=vm
	generer hostname
	SSH_CLIENT=x
	HOSTNAME=machine-distante
	POWERLINE_CHASSIS=server
	generer hostname
	HOSTNAME=conteneur
	USER=root
	POWERLINE_CHASSIS=container
	generer hostname
	capturer
}


test_02_pwd() {

	titre       "P W D"

	long_dossier="/tmp/dossier/adosssier/bdossier/rdossier/gdossier/sous/dossier"
	for d in "$PWD" "$long_dossier" ; do
		mkdir -p "$d"
		pushd "$d" &>/dev/null
		generer pwd 0
		popd &>/dev/null
	done

	capturer

	# shellcheck disable=2034
	POWERLINE_PWD_SHORTENING=ellipse
	echo
	pushd "$long_dossier" &>/dev/null
	generer pwd 0
	popd &>/dev/null

	capturer "segment-pwd-ellipse.png"

	rm -rf "/tmp/dossier"
}


test_03_python() {

	titre         "P Y T H O N"

	VIRTUAL_ENV=chemin/mon-env
	generer python
	capturer
}


test_04_maildir() {

	titre         "M A I L D I R"

	d=/tmp/test
	rm -rf "$d"
	mkdir -p "$d/new"
	touch "$d/new/"{a,b,c}

	POWERLINE_MAILDIR="$d"
	generer maildir
	capturer
}


test_05_openstack() {

	titre         "O P E N S T A C K"

	OS_USERNAME=user
	OS_PROJECT_NAME=project
	generer openstack
	capturer
}


test_06_status() {

	titre         "S T A T U S"

	generer status 1
	capturer
}

test_09_8bit() {

	titre         "8   B I T"

	unset ${!POWERLINE_*}
	POWERLINE_ICONS=powerline
	POWERLINE_PALETTE=8bit
	USER=utilisateur
	HOSTNAME=machine
	__powerline_init

	generer "hostname pwd status" 1
	capturer screenshot-8bit.png

	reset
}

test_07_archi() {

	titre         "A R C H I"

	generer archi
	capturer
}

test_07_logo() {

	titre         "L O G O"

	POWERLINE_LOGO_ID=debian
	generer logo
	POWERLINE_LOGO_ID=ubuntu
	generer logo
	POWERLINE_LOGO_ID=arch
	generer logo
	POWERLINE_LOGO_ID=fedora
	generer logo
	POWERLINE_LOGO_ID=windows
	generer logo
	capturer
}

test_07_heure() {
	titre         "H E U R E"

	generer heure
	capturer
}


test_07_jobs() {

	titre         "J O B S"

	sleep infinity &
	generer jobs
	capturer
}

test_10_git() {

	titre       "G I T"

	remote=/tmp/testgit/remote
	local=/tmp/testgit/local
	rm -rf "/tmp/testgit"
	mkdir -p "$remote"
	git init --quiet "$remote"
	git -C "$remote" commit --quiet -m "pouet" --allow-empty
	git clone --quiet "$remote" "$local"
	git -C "$remote" commit --quiet -m "pouet" --allow-empty
	pushd "$local"
	git branch --quiet --set-upstream-to=origin/master
	git config --add remote.pushDefault origin
	echo


	generer "git"
	commande git commit -m Message --allow-empty
	generer "git git_sync"
	commande git fetch
	generer "git git_sync"
	commande git checkout HEAD^
	generer "git git_sync"
	commande touch nouveau-fichier
	generer "git git_sync"
	popd >/dev/null
	capturer segment-git.png

	pushd "$local" >/dev/null
	git checkout --quiet master
	git clean --quiet -f
	echo

	POWERLINE_GIT_SYNC_COUNT=1
	generer "git git_sync"

	popd >/dev/null
	rm -rf "/tmp/testgit"

	capturer segment-git-count.png
}


test_20_docker() {

	titre         "D O C K E R"

	d="/tmp/docker"
	rm -rf "$d"
	mkdir "$d"
	pushd "$d" >/dev/null
	cat > docker-compose.yml <<-EOF
	version: "3"

	services:
	  service0:
	    image: alpine:latest
	    entrypoint: [sleep, infinity]
	EOF

	generer docker
	commande docker-compose up -d
	generer docker
	echo
	echo
	echo

	docker-compose kill
	docker-compose down -v
	popd
	rm -rf "$OLDPWD"
	capturer
}


test_20_k8s() {

	titre         "K 8 S"

	kind create cluster --name cluster
	trap "kind delete cluster --name cluster" INT EXIT TERM
	kubectl create namespace mon-namespace
	echo

	generer k8s
	commande kubectl config set-context --current --namespace mon-namespace
	generer k8s
	capturer

	kind delete cluster --name cluster
	if ! grep -q clusters ~/.kube/config ; then
		rm -f ~/.kube/config
	fi

	trap - INT EXIT TERM
}

titre() {
	# Avant chaque segment, effacer l'écran et afficher le nom du segment à
	# tester.
	clear
	echo
	echo "        $*"
	echo
}


# Compteur d'invite générée à capturer. Réinitalisé par capturer()
nombre_prompts=0
decalage_vertical=0

generer() {
	# Génère une invite de commande et l'affiche.

	# Si c'est la première invite, noter la position du curseur où
	# commencer le recadrage de la photo.
	if [ "$nombre_prompts" -eq 0 ] ; then
		echo -en "\033[6n"  # Écrit le résultat dans l'entrée standard
		read -sr -d\[ dechet
		read -sr -dR pos
		IFS=';' read -r y x <<<"$pos"
		decalage_vertical=$(( y - 1 ))
	fi

	POWERLINE_SEGMENTS="$1"
	__powerline_init
	COLUMNS=90 __update_ps1 "${2-0}"

	# Nettoyer les \[\]
	PS1="${PS1//\\]/}"
	PS1="${PS1//\\[/}"
	# Remplacer \$ par $
	PS1="${PS1/\\\$/$}"

	# Afficher l'invite, indentée de 4 espaces.
	echo -e "    ${PS1/\\n/\\n    }"

	# Incrémenter le nombre d'invite affichée à prendre en photo.
	nombre_prompts=$((nombre_prompts + 1))
}

commande() {
	# Affiche une commande dans l'invite et l'exécute silencieusement.
	tput cuu1
	tput cuf 6
	echo "$@"
	"$@" &>/dev/null || true
}

capturer() {
	# Prends une capture d'écran du terminal (fenere courante) et recadre
	# pour n'avoir que les dernières invites de commande.

	local dest="${1-segment-${POWERLINE_SEGMENTS}.png}"
	local line_count=$(( 1 + $((nombre_prompts * 2))))
	# Hauteur en pixel de l'entête de Tilix.
	local HEADER_PX=${HEADER_PX-164}
	# Décalage en bas.
	local BOTTOM_PX=${BOTTOM_PX-60}
	local line_px column_px
	local h w x y

	# Afficher quelques lignes vide sous les invites.
	echo -e '\e[0m'
	echo
	echo
	echo

	if [ -v CAPTURE ] ; then
		read -r -p "Appuyer sur ENTRÉE pour capturer dans $dest."
	else
		read -r -p "Appuyer sur ENTRÉE pour continuer."
		nombre_prompts=0
		return
	fi

	gnome-screenshot --window --delay=0 --file window.png

	IFS=x read -r w h < <(identify window.png | grep -oP '\d+x\d+(?=\+)')

	# Calculer la taille en pixel d'un caractère dans le terminal.
	column_px=$(( w / COLUMNS ))
	# Ajouter la ligne pour des onglets tmux.
	line_px=$(( (h - HEADER_PX - BOTTOM_PX) / (LINES + "0${TMUX+1}") ))

	# Calculer le recadrage.
	w=$(( 90 * column_px + 8 ))
	h=$(( line_count * line_px + 8 ))
	x=$(( 4 * column_px - 4 ))
	y=$(( (HEADER_PX - 4) + decalage_vertical * line_px ))

	convert \
		-crop "${w}x${h}+${x}+${y}" \
		window.png \
		"$docsdir/$dest"
	test -f "$_"

	rm -f window.png

	nombre_prompts=0
}


main "$@"

#!/usr/bin/env bash
set -eu

# shellcheck source=../powerline.bash
. powerline.bash

for hue in $(seq 5 4 73) ; do  # 18 teintes
	 if [ "$hue" -gt 21 ] ; then
		 hue=$((hue + 20))
	 fi
	 printf -v hue "0.%02d" $hue

	 for sat in {2..8} ; do
		 __powerline_hsl2rgb $hue "0.$sat" 0.34
		r="${__powerline_retval[0]}"
		v="${__powerline_retval[1]}"
		b="${__powerline_retval[2]}"

		printf -v texte "${hue/0.},${sat/0.} %02x%02x%02x" "$r" "$v" "$b"
		echo -en "\\e[48;2;$r;$v;$b;38;5;253m $texte \\e[0m"
	 done
	 echo
done

# `▶ powerline.bash`

`powerline.bash` est une invite de commande rapide dans le style Powerline,
pour BASH.

![capture d'écran](docs/screenshot.png)


## `▶ Fonctionnalités`

- *Installation facile*. Un seul fichier sans dépendances.
- *Très rapide*. Jusqu'à seulement 10ms pour générer l'invite. *Pas de
  serveur !*.
- Sur deux lignes pour faciliter le *copicollage*, garder le curseur au même
  endroit et laisser de la place aux longues commandes.
- `utilisateur@hôte` si connecté en sudo ou SSH, *coloré dynamiquement*.
- Dossier courant avec *dossiers intermédiaires abrégés*.
- En cas de commande en erreur, affichage du code de sortie et coloration du
  `$`.
- *Icônes* optionnelles avec [icons-in-terminal] ou [Nerd Fonts].
- De nombreux segments disponibles : git, Python, Docker, OpenStack, Maildir,
  Kubernetes, etc.
- *Configurable* par des variables shell. Pas de fichier.
- *Extensible* : rédigez une fonction bash et vous avez votre segment
  personnel.

[icons-in-terminal]: https://github.com/sebastiencs/icons-in-terminal/
[Nerd Fonts]: https://github.com/ryanoasis/nerd-fonts/
[PowerLineSymbols]: https://github.com/powerline/powerline/tree/develop/font


## `▶ Prérequis`

Bash 4.2.46 ou supérieur (RHEL7, Debian 8 Jessie).

Une locale UTF-8 configurée.

Les chevrons façon Powerline requièrent une police adaptée comme
[Nerd Fonts], [PowerLineSymbols] ou [icons-in-terminal].
Je préconnise [Nerd Fonts] qui est actif et plus populaire.

Si vous utilisez une police sans les symboles Powerline, configurez l'invite
pour avoir un mode dégradé avec `POWERLINE_ICONS=compat` ou carrément
`POWERLINE_ICONS=flat` avant de sourcer `powerline.bash` dans `.bashrc`.


## `▶ Tester & Installer`

Télécharger le fichier powerline.bash, sourcer le dans votre terminal et
activer comme suit:

``` console
$ curl -Lo ~/.config/powerline.bash https://gitlab.com/bersace/powerline-bash/raw/master/powerline.bash
$ . ~/.config/powerline.bash
$ PROMPT_COMMAND='__update_ps1 $?'
```

Si cela vous plaît, activez powerline.bash de manière définitive dans votre
`.bashrc`:

``` bash
# Configurer avant de sourcer powerline.bash:
. ${HOME}/.config/powerline.bash
PROMPT_COMMAND='__update_ps1 $?'
```

Recharger votre shell avec `exec`:

``` console
$ exec $SHELL
```


## `▶ Afficher de belles icônes`

Pour avoir des icônes dans son invite, il faut:

- Installer la police [Nerd Fonts] de votre choix si ce n'est pas déjà fait.
- Définir `POWERLINE_ICONS=nerd-fonts` avant de sourcer `powerline.bash`  dans `.bashrc`.

`POWERLINE_ICONS` accepte également la valeur `icons-in-terminal`.
Le changement de `POWERLINE_ICONS` exige le rechargement du shell.


## `▶ Segments disponibles`

Un segment est l'équivalent d'une extension de votre invite, il affiche un
texte coloré et éventuellement une icône. Voici la liste des segments
disponibles. L'ordre de cette liste est l'ordre d'affichage par défaut.

- `hostname` - nom de l'utilisateur et nom de la machine. Par défaut, activé si
  en SSH, docker, LXC, sudo ou root.

  ![Segment hostname](docs/segment-hostname.png)

  La couleur est stable.

  Si votre terminal supporte les couleurs vraies, la variable COLORTERM
  contient truecolor. Alors la couleur de ce segment a un sens. Les shell root
  sont en mauve ou rose. Les machines virtuelles en bleu, turquoise et violet.
  Les shell locaux en marron. Les shell distants en jaune ou vert.

- `archi` - Architecture de la machine. Activé sauf pour x86_64.

  ![Segment archi](docs/segment-archi.png)

- `maildir` - présence d'un courriel non-lu dans le répertoire surveillé.
  Activé si la variable `POWERLINE_MAILDIR` est définie.

  ![Segment maildir](docs/segment-maildir.png)

- `pwd` - dossier de travail courant, raccourcis. Activé par défaut.

  ![Segment pwd](docs/segment-pwd.png)

- `python` - nom du virtualenv, environment Conda ou version Pyenv. Par défaut,
  activité si python est installé.

  ![Segment python](docs/segment-python.png)

- `git` - branche courante, coloré selon état du dossier de travail. Activé si
  git est installé.

- `git_sync` - État de la synchronisation de la branche. S'il faut tirer sur la
  branche amont, une flèche descendante indique le besoin d'un `git rebase`. Si
  vous avez des commit locaux à pousser dans **votre branche**, une flèche
  montante indique le besoin d'un `git push`. Activé si git est installé.

  ![Segments git & git_sync](docs/segment-git.png)

  Si `POWERLINE_GIT_SYNC_COUNT` est défini à 1, affiche le compteur de commit
  désynchronisé.

  ![Segments git & git_sync avec compteur](docs/segment-git-count.png)

- `docker` - état du projet compose : nombre de services démarrés sur le nombre
  total de services du fichier compose parent. Activé si un docker-compose est
  installé.

  ![Segment docker](docs/segment-docker.png)

- `k8s` - contexte et namespace kubernetes courant. Activé si kubectl est
  installé et si le fichier de configuration `~/.kube/config` ou celui défini
  par `KUBE_CONFIG` existe.

  ![Segment k8s](docs/segment-k8s.png)

- `openstack` - identifiant et projet ou accès API et URL d'un fichier `openrc`
  pour OpenStack. Activité si python est installé.

  ![Segment openstack](docs/segment-openstack.png)

- `status` - code de retour d'erreur de la précédente commande. Activé par
  défaut.

  ![Segment status](docs/segment-status.png)

- `jobs` - Affiche le compteur des tâches de fond. Activé par défaut.

  ![Segment jobs](docs/segment-jobs.png)

- `etckeeper` - État d'etckeeper. Inactif par défaut.

  Il faut configurer sudo pour ne pas avoir l'invite de mot de passe :
  `[user|%groupe] ALL=(ALL:ALL) NOPASSWD: /usr/bin/etckeeper unclean`. Le texte
  du segment est affiché si la variable d'environnement
  `POWERLINE_ETCKEEPER_ICON_ONLY` est non-vide.

- `logo` - Logo du système. Requiert un terminal en couleurs vraies. Inactif
  par défaut.

  ![Segment logo](docs/segment-logo.png)

- `heure` - Affiche l'heure courante. Inactif par défaut.

  ![Segment heure](docs/segment-heure.png)

Pour supprimer a posteriori un segment autosélectionné, éditer
`POWERLINE_SEGMENTS` après avoir sourcé `powerline.bash`.

``` bash
# .bashrc
. ~/.config/powerline.bash
POWERLINE_SEGMENTS="${POWERLINE_SEGMENTS/openstack}"  # Supprimer openstack si autoconfiguré
POWERLINE_SEGMENTS="logo ${POWERLINE_SEGMENTS}"     # Ajouter le segment logo.
```


## `▶ Aller plus loin`

Pour aller plus loin dans la configuration et la personnalisation de
`powerline.bash`, le [livre de recette](docs/COOKBOOK.md) détaille le choix
d'une police, des icônes, le configuration de votre terminal préféré, et bien
plus…


## `▶ Auteurs`

Voici les bashochistes derrière ce projet :

- [Étienne BERSAC](https://gitlab.com/bersace) - mainteneur
- [Michael BIDEAU](https://gitlab.com/mbideau) - qualité, palette de couleur
- [Frédéric MEYNADIER](https://gitlab.com/fmeynadier) - segment maildir,
  support pyenv
- [Ludovic BELLIER](https://gitlab.com/ludovic.bellier) - support [Nerd Fonts],
  documentation.
- [Olivier AUDRY](https://gitlab.com/olivier.audry) - segment k8s
- [Sandile KESWA](https://github.com/skeswa) - projet original dont dérive
  powerline.bash

Les contributions sont bienvenues, quel que soit votre niveau de BASH. Un
segment partagé est optimisé et contient moins d'erreur !


## `▶ Références`

- https://github.com/b-ryan/powerline-shell
- https://github.com/ramnes/context-color
- https://github.com/skeswa/prompt


<!-- Local Variables: -->
<!-- ispell-local-dictionary: "french" -->
<!-- End: -->
